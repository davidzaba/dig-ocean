const http = require("http")
const WSClient = require("websocket").client

let client = new WSClient()

client.on("connectFailed", (err) => {
	console.log("Connection error", err.toString())
})

client.on("connect", (conn) => {
	conn.on("message", (msg) => {
		msg.type === "utf8" && (() => {
			console.log("NEW WS MESSAGE", msg.utf8Data)
		})()
	})

	conn.on("close", () => {
		console.log("Client connection closed")
	})

	conn.connected && (() => {
		conn.sendUTF("HELLO: " + Math.random().toString())
	})()
})

client.connect("ws://localhost:8080/", null)

