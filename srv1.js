const http = require("http")
const amqp = require("amqp")
const WSServer = require("websocket").server

let res;

let httpServer = http.createServer((req, _res) => {
	res = _res

	res.writeHead(200, {
    	"Content-Type": "text/html"
	})

	res.write("Hello from Host 1 **")
	res.end()
})

httpServer.listen(8085)

/**
 *
 * RabbitMQ
 */
let consumer = amqp.createConnection({ host: "165.227.143.233", port: 5672, login:"test", password: "test" })
let myExchange;
let myQueue;

consumer.on("error", (err) => console.log("ERABBITMQ RR", err))
consumer.on("ready", () => {
	myExchange = consumer.exchange("my-exch", { type: "direct" })

	consumer.queue("queue-1", (_q) => {
		myQueue = _q
		myQueue.bind(myExchange, "final-dest")
	})
})

/**
 *
 * WebSocket
 */
const wsServer = new WSServer({ httpServer })

wsServer.on("request", (req) => {
	console.log("new WS request")

	let conn = req.accept(null, req.origin)
	
	/**
	 *
	 * RabbitMQ
	 */
	 myQueue.subscribe((message) => {
		conn.sendUTF("NEW MESSAGE 1 from RabbitMQ: " + message.data.toString("utf8"))
	})

	setInterval(() => myExchange.publish("final-dest", "hello from SRV-1"), 2000)
	// -->

	conn.on("message", (msg) => {
		msg.type === "utf8" && (() => {
			//console.log("NEW WS MESSAGE", msg.utf8Data)
			conn.sendUTF("Hi, nice to see you...")
		})()
	})

	conn.on("close", (errCode, errTxt) => {
		console.log("Peer", conn.remoteAddress, "disconnected")
	})
})

