const express = require("express")

const PORT = 8087
const APP = express()
const MSG = `Service #${Date.now()} responding`

APP.get("/", (req, res) => res.send(MSG))
APP.listen(PORT)