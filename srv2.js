const http = require("http")
const amqp = require("amqp")

let consumer = amqp.createConnection({ host: "165.227.143.233", port: 5672, login:"test", password: "test" })

consumer.on("error", (err) => console.log("ERABBITMQ RR", err))
consumer.on("ready", () => {
	let _exchange = consumer.exchange("my-exch", { type: "direct" })

	consumer.queue("queue-2", (q) => {
		q.bind(_exchange, "final-dest")
		q.subscribe((message) => {
			//console.log("NEW MESSAGE 2", message.data.toString("utf8"))
		})

		setInterval(() => _exchange.publish("final-dest", "hello from SRV-2"), 2000)
	})
})

http.createServer((req, res) => {
	res.writeHead(200, {
    	"Content-Type": "text/html"
	})

	res.write("Hello from Host 2 **")
	res.end()
}).listen(8086)
